import { NgModule } from '@angular/core';
import { ClarityModule } from '@clr/angular';
import { HttpClientModule } from '@angular/common/http';
import { IpTableComponent } from './ip-table.component';
import { IpTableService } from './ip-table.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IpTableCreateOrUpdateModalComponent } from './ip-table-create-or-update.modal';

@NgModule({
  declarations: [
    IpTableComponent,
    IpTableCreateOrUpdateModalComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    ClarityModule
  ],
  providers: [
    IpTableService
  ],
  exports: [
    IpTableComponent
  ]
})
export class IpTableModule { }
