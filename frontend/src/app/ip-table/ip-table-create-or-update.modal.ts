import { Component, Input, EventEmitter, Output } from '@angular/core';
import { IpTable } from './ip-table';


@Component({
  selector: 'app-ip-table-create-or-update-modal',
  templateUrl: './ip-table-create-or-update.modal.html',
})
export class IpTableCreateOrUpdateModalComponent {

  @Input()
  ipTable: IpTable;

  @Input()
  modalOpened: boolean = false;

  @Input()
  operationInProgress: boolean = false;

  @Output()
  save: EventEmitter<IpTable> = new EventEmitter();

  @Output()
  cancel: EventEmitter<IpTable> = new EventEmitter();

  ipAddressPattern = '^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?'
                     + '[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$';

  constructor() { }

}
