import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { IpTable } from './ip-table';
import { Observable } from 'rxjs/Observable';

const IP_TABLES_URI = '/api/ip-tables';

@Injectable()
export class IpTableService {

  constructor(private http: HttpClient) { }

  public getIpTables(params: {
    ipAddress?: string,
    hostName?: string,
    pageSize?: number,
    pageNumber?: number,
    sort?: string
  } = {}): Observable<{ totalElements: number, content: IpTable[] }> {
    let httpParams: HttpParams = new HttpParams();
    if (params.ipAddress) {
      httpParams = httpParams.set('ipAddress', encodeURIComponent(params.ipAddress));
    }
    if (params.hostName) {
      httpParams = httpParams.set('hostName', encodeURIComponent(params.hostName));
    }
    if (params.sort) {
      httpParams = httpParams.set('sort', params.sort);
    }
    if (params.pageSize) {
      httpParams = httpParams.set('pageSize', params.pageSize.toString());
    }
    if (params.pageNumber !== undefined) {
      httpParams = httpParams.set('pageNumber', params.pageNumber.toString());
    }
    return this.http.get<{ totalElements: number, content: IpTable[] }>(IP_TABLES_URI, { params: httpParams });
  }

  public createIpTable(ipTable: IpTable): Observable<any> {
    return this.http.post(IP_TABLES_URI, ipTable);
  }

  public updateIpTable(ipTable: IpTable): Observable<any> {
    return this.http.put(IP_TABLES_URI, ipTable);
  }

  public deleteIpTable(id: number): Observable<any> {
    const deleteUri = IP_TABLES_URI + '/' + id;
    return this.http.delete(deleteUri);
  }

}
