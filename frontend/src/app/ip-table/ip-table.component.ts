import { Component, OnDestroy } from '@angular/core';
import { IpTableService } from './ip-table.service';
import { IpTable } from './ip-table';
import { Observable } from 'rxjs/Observable';
import { ReplaySubject } from 'rxjs/ReplaySubject';
import { AlertService } from '../alert.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Subscription } from 'rxjs/Subscription';
import { ClrDatagridStateInterface } from '@clr/angular';

@Component({
  selector: 'app-ip-table',
  templateUrl: './ip-table.component.html'
})
export class IpTableComponent implements OnDestroy {

  createOrEditModalOpened: boolean = false;
  deleteConfirmationModalOpened: boolean = false;
  selectedIpTable: IpTable;
  editableIpTable: IpTable = <IpTable>{};
  operationInProgress: boolean = false;
  loadingData: boolean = true;
  saveOrUpdateSubscription: Subscription;
  deleteSubscription: Subscription;
  ipTables: IpTable[] = [];
  totalElements = 0;
  ipTablesSubscription: Subscription;
  params: Object;
  // method alias
  cancelPreviousRequest = this.unsubsribe;


  constructor(private service: IpTableService, private alertService: AlertService) { }

  createIpTable() {
    this.createOrEditModalOpened = true;
    this.editableIpTable = <IpTable>{};
  }

  editIpTable(ipTable: IpTable) {
    this.createOrEditModalOpened = true;
    this.editableIpTable = { ...this.selectedIpTable };
  }

  deleteIpTable(ipTable: IpTable) {
    this.deleteConfirmationModalOpened = true;
  }

  onCreateOrUpdateCanceled() {
    this.createOrEditModalOpened = false;
  }

  onSave(entityToSave: IpTable) {
    this.operationInProgress = true;

    const operation = entityToSave.id
      ? this.service.updateIpTable(entityToSave)
      : this.service.createIpTable(entityToSave);

    this.cancelPreviousRequest(this.saveOrUpdateSubscription);
    this.saveOrUpdateSubscription = operation
      .finally(() => {
        this.createOrEditModalOpened = false;
        this.operationInProgress = false;
      })
      .catch(error => {
        this.alertService.emit({ type: 'ERROR', message: error.message });
        return Observable.empty();
      })
      .subscribe(() => {
        const message = entityToSave.id ? 'Record updated successfully' : 'Record created successfully';
        this.alertService.emit({ type: 'SUCCESS', message });
        this.loadData();
      });
  }

  confirmDeletion(itemToDelete: IpTable) {
    this.operationInProgress = true;

    this.cancelPreviousRequest(this.deleteSubscription);
    this.deleteSubscription = this.service.deleteIpTable(itemToDelete.id)
      .finally(() => {
        this.deleteConfirmationModalOpened = false;
        this.operationInProgress = false;
      })
      .catch((error: HttpErrorResponse) => {
        this.alertService.emit({ type: 'ERROR', message: error.message });
        return Observable.empty();
      })
      .subscribe(() => {
        this.alertService.emit({ type: 'SUCCESS', message: 'Record deleted' });
        this.loadData();
      });
  }

  ngOnDestroy(): void {
    this.unsubsribe(this.saveOrUpdateSubscription);
    this.unsubsribe(this.deleteSubscription);
    this.unsubsribe(this.ipTablesSubscription);
  }

  unsubsribe(subscription: Subscription) {
    if (subscription) {
      subscription.unsubscribe();
    }
  }

  refresh(state: ClrDatagridStateInterface) {
    const sort = state.sort && state.sort.by + (state.sort.reverse ? '.desc' : '.asc');
    const filters = state.filters
      && state.filters.reduce((acc, next: {property: string, value: string}) => {
             acc[next.property] = next.value;
             return acc;
           }, {});
    const page = state.page && { pageNumber: state.page.from / state.page.size, pageSize: state.page.size };
    const params = { ...filters, ...page, sort };
    this.loadData(params);
  }

  loadData(params?: Object) {
    if (!params) {
      params = this.params;
    }
    this.params = params;
    this.loadingData = true;
    this.ipTablesSubscription = this.service.getIpTables(this.params)
      .finally(() => this.loadingData = false)
      .catch(error => {
        this.alertService.emit({ type: 'ERROR', message: error.message });
        return Observable.of({ totalElements: 0, content: [] });
      })
      .subscribe(ipTablePage => {
        this.selectedIpTable = null;
        this.ipTables = ipTablePage.content;
        this.totalElements = ipTablePage.totalElements;
      });
  }
}
