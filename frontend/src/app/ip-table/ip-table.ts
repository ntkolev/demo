export interface IpTable {
  id: number;
  ipAddress: string;
  hostName: string;
}
