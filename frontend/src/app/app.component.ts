import { Component } from '@angular/core';
import { AlertService, Alert } from './alert.service';
import { Observable } from 'rxjs/Observable';

class ClarityAlert {

  constructor(public alertType: string, public message: string) { }

  public static of(alert: Alert): ClarityAlert {
    if ('ERROR' === alert.type ) {
      return new ClarityAlert('danger', alert.message);
    } else if ('SUCCESS' === alert.type) {
      return new ClarityAlert('success', alert.message);
    }
    // TODO: the other types
    return new ClarityAlert('info', '');
  }
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  alert$: Observable<ClarityAlert>;
  alertClosed: boolean = true;

  constructor(private alertService: AlertService) {
    this.alert$ = this.alertService.alerts$
      .do(() => this.showAlert())
      .map(ClarityAlert.of);
  }

  private showAlert(): void {
    this.alertClosed = false;
  }

}
