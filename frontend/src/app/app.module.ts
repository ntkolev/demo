import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { IpTableModule } from './ip-table/ip-table.module';

import './rxjs-operators';
import { ClarityModule } from '@clr/angular';
import { AlertService } from './alert.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    IpTableModule,
    ClarityModule
  ],
  providers: [AlertService],
  bootstrap: [AppComponent]
})
export class AppModule { }
