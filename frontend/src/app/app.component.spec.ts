import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { ClarityModule } from '@clr/angular';
import { IpTableModule } from './ip-table/ip-table.module';
import { AlertService } from './alert.service';
import './rxjs-operators';

describe('AppComponent', () => {

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        IpTableModule,
        ClarityModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        AlertService
      ],
    }).compileComponents();
  }));

  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
