import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

export class Alert {
  type: 'INFO' | 'ERROR' | 'WARNING' | 'SUCCESS' = 'INFO';
  message: string;
}

@Injectable()
export class AlertService {

  public alertsSubject: Subject<Alert> = new Subject();

  public alerts$: Observable<Alert> = this.alertsSubject.asObservable();

  public emit(alert: Alert) {
    this.alertsSubject.next(alert);
  }

}
