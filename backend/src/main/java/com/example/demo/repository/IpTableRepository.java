package com.example.demo.repository;

import com.example.demo.model.IpTable;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface IpTableRepository extends PagingAndSortingRepository<IpTable, Long> {

  Page<IpTable> findByIpAddressIgnoreCaseContaining(String ipAddress, Pageable pageable);

  Page<IpTable> findByHostNameIgnoreCaseContaining(String hostName, Pageable pageable);

  Page<IpTable> findByIpAddressContainingAndHostNameContainingAllIgnoreCase(String ipAddress, String hostName, Pageable pageable);

}
