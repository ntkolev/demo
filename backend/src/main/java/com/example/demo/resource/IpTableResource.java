package com.example.demo.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.example.demo.config.LeftPadIpAddress;
import com.example.demo.model.IpTable;
import com.example.demo.repository.IpTableRepository;

@RestController
@RequestMapping({"/api/ip-tables"})
public class IpTableResource {

    private static final int DEFAULT_PAGE_SIZE = 5;

    @Autowired
    private IpTableRepository ipTableRepository;

    @PostMapping
    @LeftPadIpAddress
    public IpTable create(@RequestBody IpTable ipTable) {
      return this.ipTableRepository.save(ipTable);
    }

    @GetMapping(path = {"/{id}"})
    public IpTable findOne(@PathVariable("id") long id) {
      final Optional<IpTable> ipTable = this.ipTableRepository.findById(id);
      if (!ipTable.isPresent()) {
        return null;
      }
      return ipTable.get();
    }

    @PutMapping
    @LeftPadIpAddress
    public IpTable update(@RequestBody IpTable ipTable) {
      return this.ipTableRepository.save(ipTable);
    }

    @DeleteMapping(path ={"/{id}"})
    public void delete(@PathVariable("id") long id) {
      this.ipTableRepository.deleteById(id);
    }

    @GetMapping
    public Page<IpTable> findAll(
      @RequestParam(name = "ipAddress", required = false) String ipAddress,
      @RequestParam(name = "hostName", required = false) String hostName,
      @RequestParam(name = "pageSize", required = false) Integer pageSize,
      @RequestParam(name = "pageNumber", required = false) Integer pageNumber,
      @RequestParam(name = "sort", required = false) List<String> sort) {
        Sort sortOrder = sort != null ? this.buildSort(sort) : null;
        Pageable pageable = this.buildPageRequest(pageSize, pageNumber, sortOrder);

        if (ipAddress == null && hostName == null) {
          return this.ipTableRepository.findAll(pageable);
        } else if (ipAddress != null && hostName != null) {
          return this.ipTableRepository.findByIpAddressContainingAndHostNameContainingAllIgnoreCase(ipAddress, hostName, pageable);
        } else if (ipAddress != null) {
          return this.ipTableRepository.findByIpAddressIgnoreCaseContaining(ipAddress, pageable);
        } else {
          return this.ipTableRepository.findByHostNameIgnoreCaseContaining(hostName, pageable);
        }
    }

    private Sort buildSort(List<String> sortQueryStrings) {
        final String[] sortProps = { "ipAddress", "hostName" };
        final String[] sortDirections = { Direction.ASC.name().toLowerCase(), Direction.DESC.name().toLowerCase() };
        List<Order> orders = sortQueryStrings.stream()
          .map(s -> s.split("\\."))
          .map(pd -> pd.length == 1 ? new String[] { pd[0], Direction.ASC.name().toLowerCase() } : pd)
          .filter(pd -> pd.length == 2)
          .filter(pd -> Arrays.asList(sortProps).contains(pd[0]))
          .map(pd -> Arrays.asList(sortDirections).contains(pd[1]) ? pd : new String[] { pd[0], Direction.ASC.name().toLowerCase() })
          .map(pd -> "ipAddress".equals(pd[0]) ? new String[] { "paddedIpAddress", pd[1] } : pd)
          .map(pd -> new Order(Direction.fromString(pd[1]), pd[0]))
          .collect(Collectors.toList());
        return Sort.by(orders);
    }

    private Pageable buildPageRequest(Integer pageSize, Integer pageNumber, Sort sort) {
      Pageable pageable = null;
      if (pageNumber != null && pageSize == null) {
        pageable = PageRequest.of(pageNumber, DEFAULT_PAGE_SIZE, sort);
      } else if (pageSize != null && pageNumber == null) {
        pageable = PageRequest.of(0, pageSize, sort);
      } else if (pageNumber != null && pageSize != null) {
        pageable = PageRequest.of(pageNumber, pageSize, sort);
      }
      if (pageable == null) {
        pageable = PageRequest.of(0, DEFAULT_PAGE_SIZE, sort);
      }
      return this.removeNullSort(pageable);
    }

    private Pageable removeNullSort(Pageable pageable) {
      if (pageable.getSort() == null) {
        return PageRequest.of(pageable.getPageNumber(), pageable.getPageSize());
      }
      return pageable;
    }

}
