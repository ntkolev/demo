package com.example.demo.resource;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import com.example.demo.model.IpTable;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class LeftPadIpAddressAspect {

  private final String ipv4RegexPattern = "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$";

  private final Pattern ipv4Pattern = Pattern.compile(ipv4RegexPattern);

  @Before("@annotation(com.example.demo.config.LeftPadIpAddress)")
  public void before(JoinPoint joinPoint) {
    final Object[] arguments = joinPoint.getArgs();
    if (arguments.length == 1 && arguments[0] instanceof IpTable) {
      IpTable ipTable = (IpTable)arguments[0];
      if (!this.validateIPv4Addres(ipTable.getIpAddress())) {
        throw new RuntimeException("Invalid IPv4 Address");
      }
      final String paddedIpAddress = this.leftPadIpAddress(ipTable.getIpAddress());
      ipTable.setPaddedIpAddress(paddedIpAddress);
    } else {
      throw new RuntimeException("@LeftPadIpAddress should be applied on a method with single IpTable input parameter.");
    }
  }

  private String leftPad(String source, int length) {
    return String.format("%0" + length + "d", Integer.parseInt(source));
  }

  private String leftPadIpAddress(String ipAddress) {
    final String[] ipAddressParts = ipAddress.split("\\.");
    final String[] paddedIpAddressParts = Stream.of(ipAddressParts)
      .map((s) -> this.leftPad(s, 3))
      .toArray(String[]::new);
    return String.join(".", paddedIpAddressParts);
  }

  private boolean validateIPv4Addres(String candidate) {
    return this.ipv4Pattern.matcher(candidate).find();
  }

}
