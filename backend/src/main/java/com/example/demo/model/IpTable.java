package com.example.demo.model;

import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;


@Entity
public class IpTable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @NotNull
    @Pattern(
      regexp = "^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$",
      message = "Invalid IPv4 address")
    private String ipAddress;

    @NotNull
    private String paddedIpAddress;

    @NotNull
    private String hostName;

    public long getId() {
      return this.id;
    }

    public void setId(long id) {
      this.id = id;
    }

    public String getIpAddress() {
      return this.ipAddress;
    }

    public void setIpAddress(String ipAddress) {
      this.ipAddress = ipAddress;
    }

    public String getPaddedIpAddress() {
      return this.paddedIpAddress;
    }

    public void setPaddedIpAddress(String paddedIpAddress) {
      this.paddedIpAddress = paddedIpAddress;
    }

    public String getHostName() {
      return this.hostName;
    }

    public void setHostName(String hostName) {
      this.hostName = hostName;
    }

    public int hashCode() {
      return Objects.hash(this.ipAddress, this.paddedIpAddress, this.hostName);
    }

    public boolean equals(Object o) {
      if (!(o instanceof IpTable)) {
        return false;
      }
      IpTable other = (IpTable)o;
      return Objects.equals(this.ipAddress, other.ipAddress)
        && Objects.equals(this.paddedIpAddress, other.paddedIpAddress)
        && Objects.equals(this.hostName, other.hostName);
    }

}
