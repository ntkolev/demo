create table IF NOT EXISTS ip_table (
  id bigserial not null,
  ip_address varchar(45) not null,
  padded_ip_address varchar(45) not null,
  host_name varchar(500) not null,
  primary key (id)
);

INSERT into ip_table(ip_address, padded_ip_address, host_name)
  values('74.125.224.72', '074.125.224.72', 'google.com');
INSERT into ip_table(ip_address, padded_ip_address, host_name)
  values('103.21.244.0/22', '103.021.244.000/22', 'cloudflare.com');
INSERT into ip_table(ip_address, padded_ip_address, host_name)
  values('127.0.0.1', '127.000.000.001', 'localhost');
