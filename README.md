# Danaos Demo

## Project Structure

`backend` contains the spring boot application and REST API

`frontend` contains the Angular 5 application, scaffolded using angular-cli

## Build and Run

Open `backend\src\main\resources\application.properties` and modify the database connection patameters as needed.

The application makes use of `liquibase` and when run it will create an `ip_table` table and populate it with some rows.

Execute the following commands in the root of the project:

`./mvnw clean install`

`cd backend/target && java -jar demo-backend-0.0.1-SNAPSHOT.jar`

Open the browser at http://localhost:8080 and log in using username: `user` and password: `password`

## Development

Open a terminal, navigate to the root of the projet and execute

`cd backend && ../mvnw spring-boot:run`

in order to start the backend server. It will be available at `http://localhost:8080`

Open another terminal, navigate to the root of the projects and execute

`cd fronted && npm run start`

This command will run the development server of the frontend, which makes the application available at `http://localhost:4200`. The development server will proxy all api requests to `http://localhost:8080` so you need to have that running.

Open the browser, navigate to `http://localhost:8080` and log in. After that you can use the same browser window to open `http://localhost:4200` in another tab. The session will be shared.
